def head_add_one(x):
	new_x = x
	new_x[0] += 1
	return new_x

def reset_x_list(x):
	x = [0] * len(x)

if __name__ == '__main__':
	x0 = [1,2,3]

	x1 = head_add_one(x0)
	if x1[0] == x0[0]+1:
		print("pass test 1")

	reset_x_list(x0)
	if sum(x0) == 0:
		print("pass test 2")
